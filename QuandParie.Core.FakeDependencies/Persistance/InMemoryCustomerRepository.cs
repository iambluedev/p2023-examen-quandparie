﻿using QuandParie.Core.Domain;
using QuandParie.Core.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.FakeDependencies.Persistance
{
    public class InMemoryCustomerRepository : ICustomerRepository
    {
        public Dictionary<string, Customer> Customers = new();

        public void Save(Customer customer)
            => Customers[customer.Email] = customer;

        public Task<Customer> GetAsync(string email)
        {
            if (!Customers.TryGetValue(email, out var result))
                result = null;
            return Task.FromResult(result);
        }

        public Task SaveAsync(Customer customer)
        {
            Save(customer);
            return Task.CompletedTask;
        }
    }
}
