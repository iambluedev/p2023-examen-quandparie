using QuandParie.Core.ReadOnlyInterfaces;
using System;

namespace QuandParie.Core.Domain
{
    public class Odds : IReadOnlyOdds
    {
        public Guid Id { get; } = Guid.NewGuid();
        public string Match { get; }
        public string Condition { get; }

        private decimal value;
        public decimal Value
        {
            get => value;
            set
            {
                if (value < 1)
                    throw new ArgumentOutOfRangeException(nameof(value), value,
                        $"The value of some odds must be superior (or equal) than 1");

                if (outcome != null)
                    throw new InvalidOperationException("Cannot change the value of some odds once the outcome has been set");

                this.value = value;
            }
        }

        private bool? outcome;
        public bool? Outcome
        {
            get => outcome;
            set
            {
                if (outcome != null && outcome != value)
                    throw new InvalidOperationException("Cannot change the outcome of some odds once the outcome has been set");

                outcome = value;
            }
        }

        public bool CanBeWagered => Outcome == null;

        public Odds(string match, string condition, decimal value)
        {
            Match = match;
            Condition = condition;
            Value = value;
        }
    }
}