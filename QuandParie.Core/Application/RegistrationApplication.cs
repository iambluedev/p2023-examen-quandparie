﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using QuandParie.Core.Persistance;
using System.Threading.Tasks;
using QuandParie.Core.ReadOnlyInterfaces;

namespace QuandParie.Core.Application
{
    public class RegistrationApplication
    {
        // TODO 2006-12-23: Aymeric essayes de mettre de meilleur noms
        // TODO 2007-02-04: Rah, c'est bon, c'est très bien comme ça, le code est concis
        // TODO 2012-05-18: Added documentation, but we need to complete them
        // TODO 2019-07-12: Find better names when I get some time
        // TODO 2022-01-07: On fait ce qu'on peut avec ce qu'on a 😬

        private readonly ICustomerRepository customerRepository;

        private readonly IDocumentRepository documentRepository;

        private readonly IIdentityProofer identityProofer;

        private readonly IAddressProofer addressProofer;

        public RegistrationApplication(
            ICustomerRepository customerRepository, 
            IDocumentRepository documentRepository, 
            IIdentityProofer identityProofer,
            IAddressProofer addressProofer)
        {
            this.customerRepository = customerRepository;
            this.documentRepository = documentRepository;
            this.identityProofer = identityProofer;
            this.addressProofer = addressProofer;
        }

        public async Task<IReadOnlyCustomer> CreateAccount(string email, string firstName, string lastName)
        {
            var customer = new Customer(email, firstName, lastName);
            await customerRepository.SaveAsync(customer);

            return customer;
        }

        public async Task<IReadOnlyCustomer> GetAccount(string email)
        {
            return await customerRepository.GetAsync(email);
        }

        public async Task<bool> UploadIdentityProof(string email, byte[] data)
        {
            var customer = await customerRepository.GetAsync(email);
            if (!identityProofer.Validates(customer, data))
                return false;

            await documentRepository.SaveAsync(DocumentType.IdentityProof, email, data);

            customer.IsIdentityVerified = true;
            await customerRepository.SaveAsync(customer);

            return true;
        }

        public async Task<bool> UploadAddressProof(string email, byte[] data)
        {
            var customer = await this.customerRepository.GetAsync(email);
            if (!addressProofer.Validates(customer, out var address, data))
                return false;

            await documentRepository.SaveAsync(DocumentType.AddressProof, email, data);

            customer.Address = address;
            await this.customerRepository.SaveAsync(customer);

            return true;
        }
    }
}
